(function($){

$.each(["addClass","removeClass"],function(i,methodname){
      var oldmethod = $.fn[methodname];
      $.fn[methodname] = function(){
            oldmethod.apply( this, arguments );
            this.trigger(methodname+"change");
            return this;
      }
});

})(jQuery);


/**
 *	jQuery mutationObserver 1.0.2
 *	https://github.com/timbonicus/jquery-mutationobserver
 *
 *	Dual licensed under the MIT and GPL licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 */
(function($) {
    var jQueryMutationFns = ['after', 'append', 'before', 'empty', 'html', 'prepend', 'remove']
    var mutationObservers = []
    var __mutationTimeout

    $.each(jQueryMutationFns, function(i, fn) {
        var originalFn = $.fn[fn]
        $.fn[fn] = function() {
            var me = this
            var result = originalFn.apply(this, arguments)
            clearTimeout(__mutationTimeout)
            __mutationTimeout = setTimeout(function() { fire(me) }, 50)
            return result
        }
    })

    function fire($element) {
        $.each(mutationObservers, function(i, observer) {
            if ($element.closest(observer.el).length)
                observer.listener()
        })
    }

    $.fn.mutationObserver = function(listenerFn) {
        if (this.length == 0)
            return this
        if (this.length > 1)
            return $.each(this, function(i, obj) { $(obj).mutationObserver(listenerFn) })

        mutationObservers.push({el: this, listener: $.proxy(listenerFn, this)})
    }
}(jQuery));




var ElementQuery = {
	_init:false,
	listenerFn:null,
	mediaQueries:[],
	throttledApply:null,
	init:function(){
		if(this._init == true){
			return false;
		}

		this.throttledApply = _.throttle(ElementQuery.applyMediaQueries, 100);

		this.listenerFn = function() {
			ElementQuery.throttledApply();
		};
		

		$(window).resize(function(){
			ElementQuery.throttledApply();
		});

		$(document).ready($.proxy(function(){
			$("body").mutationObserver(this.listenerFn);
		},this));

		this._init = true;

	},



	add:function(options){
		this.init();

		this.mediaQueries.push(options);

		$(options.selector).bind('attrchange csschange',function(event){
		     ElementQuery.throttledApply();
		});
		this.applyMediaQuery(options);

	},

	applyMediaQuery:function(options){

 		$(options.selector).each(function(index, el){
 			$element = $(el);
	 		if(options["this-min-width"] != undefined){
	 			if($element.width() >= parseInt(options["this-min-width"]) && $element.is(':visible')){
	 				$element.addClass(options["class"]);
	 			}else if($element.is(':visible')){
	 				$element.removeClass(options["class"]);
	 			}
	 		}else if(options["parent-min-width"] != undefined){
	 			if($element.parent().width() >= parseInt(options["parent-min-width"]) && $element.is(':visible')){
	 				$element.addClass(options["class"]);
	 			}else if($element.is(':visible')){
	 				$element.removeClass(options["class"]);
	 			}
	 		}
 			
 		});
	},

	applyMediaQueries:function(){
		_.each(this.mediaQueries, function(mediaQuery){
			this.applyMediaQuery(mediaQuery);
		},this);	
	}

};